remotesubmit
============

can submit my hadoop job remotely

##如果要将github当做一个maven仓库，下面的内容加入到~/.m2/setting.xml里##
```xml
  <server>
      <id>github</id>
      <username>your-github-user-email</username>
      <password>*****</password>
    </server>
```

##引用github上的maven倉庫內容##
```xml
<repositories>
    <repository>
        <id>YOUR-PROJECT-NAME-mvn-repo</id>
        <url>https://raw.github.com/YOUR-USERNAME/YOUR-PROJECT-NAME/mvn-repo/</url>
        <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
        </snapshots>
    </repository>
</repositories>
```

```xml
<!-- 下面的要加入到~/.m2/setting.xml里,如果你要发布到sonatype-nexus -->
<servers>
	<server>
		<id>sonatype-nexus-snapshots</id>
		<username>your-sonatype-user-name</username>
		<password>****</password>
	</server>
</servers>
```

##sonatype仓库地址##
```xml
<repository>
	<id>maven_central_release</id>
	<name>Central Release</name>
	<url>https://oss.sonatype.org/content/repositories/releases/</url>
</repository>

```

##总结##
- site-maven-plugin的作用是利用github的api接口，自动deploy需要的maven jar
- maven-deploy-plugin里需要配置另外的maven jar发布位置
- maven-deploy-plugin https://maven.apache.org/plugins/maven-deploy-plugin/
- sonar 进行代码质量检测http://my.oschina.net/codingforme/blog/185106

